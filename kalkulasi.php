<?php 
require "functions/penjumlahan.php";
require "functions/pengurangan.php";
require "functions/pembagian.php";
require "functions/perkalian.php";
require "functions/perpangkatan.php";

$pertama = $_GET['pertama'];
$kedua = $_GET['kedua'];
$operator = $_GET['operator'];

if($operator === '+'){
	$hasil = penjumlahan($pertama, $kedua);
	echo "Hasil : ".$hasil;
}
elseif($operator === '-'){
	$hasil = pengurangan($pertama, $kedua);
	echo "Hasil : ".$hasil;
}
elseif($operator === ':'){
	if($kedua == 0){
		$hasil = "Maaf, untuk pembagian, angka kedua tidak boleh nol. ";
	}
	else{
		$hasil = pembagian($pertama, $kedua);
	}

	echo "Hasil : ".$hasil;
}
elseif($operator === 'x'){
	$hasil = perkalian($pertama, $kedua);
	echo "Hasil : ".$hasil;
}
elseif($operator === '^'){
	$hasil = perpangkatan($pertama, $kedua);
	echo "Hasil : ".$hasil;
}

echo "<br><br><a href='index.php'>Kembali</a>";



?>